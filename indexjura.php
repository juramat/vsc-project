<?php include('header1.php'); ?>


<div class="parallax-container jurosContainer">
   <div class="parallax">
        <img src="images/parallax1.jpg" class="imagess"></div>

    <div class="jurosText">
      <h5 class="title1">Just fabulous!</h5>
    	<h2 class="title0">We serve quality food</h2>   	
    	<h5 class="title1">Mr. Pub is a great place to meet beloved friends, drink exquisite beer and eat fancy things!</h5>
      <a class="waves-effect brown btn">Order beer!</a>
      <a class="waves-effect brown btn">Order kebab!</a>
    </div>
</div>

<div class="carousel juraCarousel carousel-slider center " data-indicators="true">
    <div class="carousel-fixed-item center">
      <a class="btn waves-effect brown">Order now!</a>
    </div>
    <div class="carousel-item  brown lighten-1 white-text" href="#one!">
      <h3 class="title1">For meat lovers</h3>
      <h6 class="white-text">Fabulous smell n taste.</h6>
      <div class="meat food">
        <div class="food1">
          <h5 class="title1">Smoked Beef with Paprika</h5>
          <p>Roast trout, English asparagus, watercress and royals, creamy chesapeake crab dip with artichoke, baked and topped with cheddar cheese, with crusty bread for dipping.</p>
          <img src="images/meat1.jpg">      
        </div>
        <div class="food1">
          <h5 class="title1">Sea Bull</h5>
          <p>Creamy chesapeake crab dip with artichoke, baked and topped with cheddar cheese. With crusty bread for dipping. Creamy chesapeake crab dip with artichoke, baked and topped with cheddar cheese.</p>
          <img src="images/meat2.jpg">
        </div>
        <div class="food1">
          <h5 class="title1">Pink Unicorn</h5>
          <p>Monkfish, onion, paella rice, garlic and smoked paprika, creamy chesapeake crab dip with artichoke, baked and topped with cheddar cheese.</p>
          <img src="images/meat3.jpg">    
        </div>
      </div>    
    </div>
    <div class="carousel-item  brown lighten-1 white-text" href="#two!">
      <h3 class="title1">Vegetarian</h3>
      <h6 class="white-text">Vegan stuff you will like!</h6>
      <div class="veg food">
        <div class="food1">
          <h5 class="title1">Grilled Veggies</h5>
          <p>Monkfish, onion, paella rice, garlic and smoked paprika, creamy chesapeake crab dip with artichoke, baked and topped with cheddar cheese.</p>
          <img src="images/meat1.jpg">      
        </div>
        <div class="food1">
          <h5 class="title1">Amazing Shrooms</h5>
          <p>Creamy chesapeake crab dip with artichoke, baked and topped with cheddar cheese. With crusty bread for dipping. Creamy chesapeake crab dip with artichoke, baked and topped with cheddar cheese.</p>
          <img src="images/meat2.jpg">
        </div>
        <div class="food1">
          <h5 class="title1">Triple fish fingers</h5>
          <p>Roast trout, English asparagus, watercress and royals, creamy chesapeake crab dip with artichoke, baked and topped with cheddar cheese, with crusty bread for dipping.</p>
          <img src="images/meat3.jpg">    
        </div>
      </div> 
    </div>
    <div class="carousel-item  brown lighten-1 white-text" href="#three!">
      <h3 class="title1">Snacks for party</h3>
      <h6 class="white-text">Mucho great. Such fab.</h6>
      <div class="party food">
        <div class="food1">
          <h5 class="title1">This dish is great</h5>
          <p>Roast trout, English asparagus, watercress and royals, creamy chesapeake crab dip with artichoke, baked and topped with cheddar cheese, with crusty bread for dipping.</p>
          <img src="images/meat1.jpg">      
        </div>
        <div class="food1">
          <h5 class="title1">This one is better</h5>
          <p>Roast trout, English asparagus, watercress and royals, creamy chesapeake crab dip with artichoke, baked and topped with cheddar cheese, with crusty bread for dipping.</p>
          <img src="images/meat2.jpg">
        </div>
        <div class="food1">
          <h5 class="title1">Yes, indeed</h5>
          <p>Roast trout, English asparagus, watercress and royals, creamy chesapeake crab dip with artichoke, baked and topped with cheddar cheese, with crusty bread for dipping.</p>
          <img src="images/meat3.jpg">    
        </div>
      </div> 
    </div>
</div>
<div class="cont1" style="border-bottom: solid 1px rgba(115, 77, 38,0.5)">
  <div class="text">
    <h6 class="brown-text text-lighten-3">Fab as fu*k experience</h6>
    <h4 class="title4">Let the tongue feel the vibes</h4>    
    <h6 class="brown-text text-lighten-3">We offer the best ingrediens with obviously outstanding quality!</h6>
  </div>
  <div class="row">     
      <div class="col s12 m6 l6 xl6" style="border-right: solid 1px rgba(115, 77, 38,0.5)">
      <h4 class="price title4">$29.95</h4>
      <div class="ft z-depth-4"><img class="materialboxed l6 xl6" src="images/sample-1.jpg" ></div>
      <p>Mint parsley with apple cider vinegar, salt n spices crab dip with artichoke, baked and topped with cheddar cheese, with crusty bread for dipping. creamy chesapeake crab dip with artichoke, baked and topped with cheddar cheese.</p>
      </div>
    
      <div class="col s12 m6 l6 xl6">
      <p>Roast trout, English asparagus, watercress  royals, creamy chesapeake crab dip with artichoke, baked and topped with cheddar cheese, with crusty bread for dipping. creamy chesapeake crab dip with artichoke, baked and topped with cheddar cheese.</p>
      <div class="ft z-depth-4"><img class="materialboxed l6 xl6" src="images/minimeal.jpg "></div>
      <h4 class="price title4">$19.85</h4>
      </div>
      <a class="btn waves-effect brown">See ful menu, hun!</a>
  </div>
</div>



 <div class="meals cont1">
    <h3 class="title4">Our events are over 9000!</h3>
    <h6 class="white-text">Enjoy them, m8</h6>
    <div class="owl-carousel" style="text-align: left;">  
      <div class="meals1 "><!-- z-depth-1 col s12 m6 l3 xl3 -->
        <img src="http://img.sndimg.com/food/image/upload/w_555,h_416,c_fit,fl_progressive,q_95/v1/img/recipes/92/02/1/8wimK3LVR5yUgXYUZImK_DSC_0473-2.jpg" class="z-depth-4"> 
        <p class="title4">Roast trout, English asparagus, watercress  royals.</p>  
        <p class="meal2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco.</p> 
        <p class="title4">December 25, 2017</p>    
      </div>
      <div  class="meals1">
        <img src="http://www.corkforest.org/wordpress/wp-content/uploads/2014/03/footer-food-img.jpg" class="z-depth-4">
        <p class="title4">Roast trout, English asparagus, watercress  royals.</p>
        <p class=" meal2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco.</p> 
        <p class="title4">May 25, 2017</p>
      </div>
      <div  class="meals1 ">
        <img src="https://agileui.com/demo/delight/assets/image-resources/stock-images/img-24.jpg" class="z-depth-4">
        <p class="title4">Roast trout, English asparagus, watercress  royals.</p>
        <p class=" meal2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco.</p> 
         <p class="title4">February 25, 2017</p>  
      </div>
      <div  class="meals1 ">
        <img src="http://img.sndimg.com/food/image/upload/w_555,h_416,c_fit,fl_progressive,q_95/v1/img/recipes/92/02/1/8wimK3LVR5yUgXYUZImK_DSC_0473-2.jpg" class="z-depth-4">
        <p class="title4">Roast trout, English asparagus, watercress  royals.</p>
        <p class=" meal2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco.</p>
        <p class="title4">June 29, 2017</p>
      </div>
      <div  class="meals1 ">
        <img src="http://www.corkforest.org/wordpress/wp-content/uploads/2014/03/footer-food-img.jpg" class="z-depth-4"> 
        <p class="title4">Roast trout, English asparagus, watercress  royals.</p>
        <p class=" meal2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco.</p> 
        <p class="title4">May 25, 2017</p>
      </div>
    </div>
  </div>
<script src="jscriptjura.js" type="text/javascript"></script>

<?php include('footer.php'); ?>
