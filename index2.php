<?php include('header1.php'); ?>


<div class="parallax-container nervasima">
   <div class="parallax">
        <img class="karolioContainer" src="images2/byra.jpg"></div>

    <div class="karolioText">
      <h5 class="title1 colorWhite">"The difference between a beer and your opinion is that I asked for a beer."</h5>
      <h2 class="title0 colorWhite">Popular and rare ales & lagers at our place</h2>     
      
     <a href="index200.php" target="_blank" class='waves-effect brown btn btn2'>Ales</a>

     <a href="index20.php" target="_blank" class="waves-effect brown btn btn2">Lagers</a>
    
    </div>
</div>

<div>
 <div class="parallax-container nervasima2">
 <div class="parallax karolioContainer2">
        <img class="karolioContainer" src="images2/elbironi.jpg"></div>

<div class="karolioText2">
<h5 class="title4 colorBrown">There are many beer types in the world and it’s not an easy task to classify them by some common criteria. Beer can be grouped by colour, strength, origin and many other characteristics. However, the main and most widely used classification is according to the fermentation method. So, beer is divided into two main branches: ales and lagers. 
  </h5>
  </div>

<div class="col1 karolioText2 title4 colorBrown">                          
    <h3>Ale</h3> 
    <p class="par1 karolioText2 title4 colorBrown">Ale fermentation takes place at high temperature and yeast ferments wort on top of the tank. Shorter fermentation at higher temperatures usually gives tangier, richer flavour and higher concentration of alcohol. Its sweet taste remains, and this characterized by the body of butter, aroma and flavour complexity, and fruitiness. Flavour characteristics are revealed the best when it is served at 4-10 °C. Production of ales is popular around the world, but the Brits are most famous in this field. Barley is commonly used in its manufacture, but it is often complemented with other grains: wheat, rye, oats. Sometimes fruits, herbs and other seasonings are added to make different taste every time.
    <a href="index200.php" target="_blank">More about our ales...</a>
</p>                         
</div>

<div class="col2 karolioText2 title4 colorBrown">
    <h3>Lager</h3>  
    <p class="par2 karolioText2 title4 colorBrown">This type is known in Bavaria from the seventeenth century, when beer was made by monks who were keeping it in cool cellars, because, compared to ale, lager requires lower fermentation temperature (5-9 degrees Celsius). This is fermentation beer, where after the fermentation yeast is deposited at the bottom of the barrel, and thus a clear beer is made. Even the name of beer comes from aging in cellars (Ger. Lagern – to protect, keep). Many lagers are light, but depending on the malt they can range from pale yellow (straw) to black. Lager beer is usually made from barley. Flavour made by hops can range from bitter to sweetish. It is characterized by flavour “smoothness” and purity. Mostly it is clear and crisp beer, with light taste, and currently the most popular beer not only in Lithuania, but also around the world, accounting for about 85% of all beers sold worldwide. 
    <a href="index20.php" target="_blank">More about our lagers...</a>

</p>                      
</div>

</div>

  </div>


<?php include('footer.php'); ?>

