  <footer class="page-footer brown">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h3 class="white-text brand-logo ">Mr.Pub</h3>
                <h5 class="grey-text text-lighten-4 title4">Most fabulous place to visit!</h5>
              </div>
              <div class="col l4 offset-l2 s12 footer-contacts">
                <h5 class="white-text">Contact us!</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!">Visit Facebook!</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Visit Instagram!</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Visit Stuff!</a></li>
                </ul>
              </div>
            </div>
          </div>

        </footer>
<!--jquery-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
 <!-- Compiled and minified JavaScript -->

<!-- owl carousel -->
<script src="owlcarousel/owl.carousel.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/js/materialize.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script>
<!--jquery mani-->
<script src="jscriptjura.js" type="text/javascript"></script>

        </body>
</html>


  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/js/materialize.min.js"></script> -->
  <!-- <script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script> -->
