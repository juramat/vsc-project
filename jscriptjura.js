 //navbar
 $( document ).ready(function(){
 	$(".button-collapse").sideNav();
 })
 //paralax/*
 $(document).ready(function(){
      $('.parallax').parallax();
});
 
 //carousel
 $('.carousel.carousel-slider').carousel({fullWidth: true});
  //owl carousel
 $(document).ready(function(){
  $(".owl-carousel").owlCarousel();
});


$(document).ready(function(){
   $('.carousel').carousel();
});
$(document).ready(function(){
    $('.slider').slider();
    // Pause slider
    $('.slider').slider('pause');
    // Start slider
    $('.slider').slider('start');
    // Next slide
    $('.slider').slider('next');
    // Previous slide
    $('.slider').slider('prev');
});