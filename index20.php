<?php include('header1.php'); ?>

<div>
 <div class="parallax-container imgedit">
 <div class="parallax parallaxBeer">
 </div>
   <div class="aleshead title4 colorBrown">                 
    <h3>Our lagers</h3>                       
</div>
<div class="alestext title4 colorBrown">
	<h4>Enjoy a variety of exquisite lagers at Mr.Pub. Here's some of our lagers that we serve to our people.
	</h4>
</div>

<div class="alestext2 title4 colorBrown">
	<h5>American Lager</h5>
	<p class="par3 karolioText2 title4 colorBrown">Medium body beer ranging from pale straw to gold colour. The taste is complex, with a perfect balance of malt and hops, ending in a dry note. Light fruitiness can be felt. Malt sweetness from mild to moderate. Quite aggressive carbonation. Corn, rice and other grains are often used, resulting in the brighter colour of beer, but the lighter taste. American lagers are good with American dishes – pizza, fried chicken wings, burgers, as well as matured cheese, mussels, spicy cashew and Latin American dishes. 
</p>
<img src="images2/americanlager.png" class="myImg1">
<img src="images2/americanlager2.png" class="myImg1 myImg101">
<img src="images2/americanlager3.png" class="myImg1 myImg102">
<img src="images2/americanlager4.png" class="myImg1 myImg103">
</div>
<hr>
<div id="eurolag" class="alestext3 title4 colorBrown">
	<h5>European Lager</h5>
	<p class="par3 karolioText2 title4 colorBrown">This type of beers is very popular around the world for a very light taste. Excellent harmony of malt aroma and mild hop bitterness. Colour – from clear amber to glassy copper. The taste of European-dark lager primarily reveals the malt flavour aroma of fruits and malt, with sweetness aftertaste balanced by hops. European pale lager also has medium body. Traditional light Lithuanian beer ranges from glassy yellow to light bronze colour. In this beer you can feel gentle combination of malt and hops, it is lightly carbonized. Can be both transparent and unfiltered. European strong lager is a stronger version of commercial lager, without too much changing of its taste. Gentle aroma of hops, hardly felt malt.
</p>
<img src="images2/eurolager.png" class="myImg2">
<img src="images2/eurolager2.png" class="myImg2">
<img src="images2/eurolager3.png" class="myImg2">
</div>
<hr>
<div class="alestext2 title4 colorBrown">
	<h5>Pilsner</h5>
	<p class="par3 karolioText2 title4 colorBrown">Popular golden lager type. This is one of the youngest beer types in the world, created in 1842 in the Czech town of Pilsen. There beer is made from a soft local water, light malt and aromatic hops. Pilsner typically has 4-5% strength, durable white froth, intensive hop bitterness and is especially popular in hot summer. Strong advertising hop aroma and bitterness are mandatory for this type. Strong malt flavour is notable in the aroma and taste, and the taste is characterized by richness. It can be used with salad, salmon, chicken, as well as with spicy dishes from the Mexican cuisine.
</p>
<img src="images2/pilsner.png" class="myImg1 myImg1over">
<img src="images2/pilsner2.png" class="myImg1 myImg1over1">
</div>
<hr>
<div class="alestext3 title4 colorBrown">
	<h5>German Lager</h5>
	<p class="par3 karolioText2 title4 colorBrown">

Germany is a country preserving deep beer brewing traditions and the birthplace of lager beer. More than five hundred years ago the Bavarian brewers invented the bottom fermentation brewing method which eventually spread throughout the world. the German type lager yeast is designed to highlight the character of malts. South German and Bavarian lager yeast often produces a larger amount of diacetyl (diacetyl is a fermentation by-product, with a strong butter taste). Oktoberfest beer is made of yeast which highlights the malt to the extreme, leaving the hop character in the background. Light German lager is brewed using light Pilzen or stronger Munich malt, therefore it usually has a golden colour. Dark lager is made by adding additional caramel or chocolate and black malt to the main malt.
</p>
<img src="images2/germanlager.png" class="myImg2 germanlager">
<img src="images2/germanlager2.png" class="myImg2 germanlager">
<img src="images2/germanlager3.png" class="myImg2 germanlager">
<img src="images2/germanlager4.png" class="myImg2 germanlager">
</div>
</div>
</div>



 <!--jquery-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
 <!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/js/materialize.min.js"></script>
<!--jquery mani + savo ikelkit-->
<script src="jscriptjura.js" type="text/javascript"></script>
</body>
</html>

<?php include('footer.php'); ?>