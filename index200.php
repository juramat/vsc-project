<?php include('header1.php'); ?>

<div>
 <div class="parallax-container imgedit">
 <div class="parallax parallaxBeer">
 </div>
   <div class="aleshead title4 colorBrown">                 
    <h3>Our ales</h3>                       
</div>
<div class="alestext title4 colorBrown">
	<h4>Indulge yourself in tasting our finest ales from all over the world. Here's some of the remarkable ales at Mr.Pub that we share with our fellow ale-lovers.
	</h4>
</div>

<div class="alestext2 title4 colorBrown">
	<h5>Belgian Ale</h5>
	<p class="par3 karolioText2 title4 colorBrown">Most Belgian beers are strong, but have relatively mild flavour. Many of them are still brewed according to old monastic recipes, for example, Trappist beers are brewed in monasteries that are still functioning. The taste is usually light fruity, not so bitter, but can range from clearly expressed chocolate malt in dark varieties. Colour also varies from light to dark brown. Medium hard body with a long lasting aftertaste. Clear flavours of sweet and sour citrus fruit, but do not be surprised if you feel the notes of cumin, cloves and coriander. The variety of aromas and flavours comes not only from malt and hops, but also from extra spice.

</p>
<img src="images2/belgianale.png" class="myImg1">
<img src="images2/belgianale2.png" class="myImg1 myImg101">
<img src="images2/belgianale3.png" class="myImg1 myImg102">
<img src="images2/belgianale4.png" class="myImg1 myImg103">
</div>
<hr>
<div id="amlag" class="alestext3 title4 colorBrown">
	<h5>Lambic</h5>
	<p class="par3 karolioText2 title4 colorBrown">The beer’s name comes from the name of the Belgian town of Lembeek. Lambic beer fermentation is carried out under non-sterile conditions with carefully selected yeast species, but is left to nature. Made from barley and wheat malts (no more than 40% wheat). Big amount of hops are added to lambic beer – not only for bitterness, but also for preservation, because they suppress the proliferation of undesirable microorganisms. Beer is fermented in barrels, where in a year or two beer turns in a beer with dry, subtle sour taste and rich aroma. Young lambic beer is lightly carbonated, slightly acidic, and dry. When it is matured longer, it becomes zesty, with the highlights of tannins. This beer is somewhat akin to a dry wine or cider. Before the second fermentation (approximately in six months) fruit or berries (cherry, raspberry, peach, black currant and even apple) are usually added to the fruit type of beer. As berries are added during fermentation, sugar contained in them is consumed, therefore beer takes fruit flavour without becoming sweet.

</p>
<img src="images2/lambic.png" class="myImg3">
<img src="images2/lambic2.png" class="myImg3">
<img src="images2/lambic3.png" class="myImg3">
<img src="images2/lambic4.png" class="myImg3">
</div>
<hr>
<div class="alestext2 title4 colorBrown">
	<h5>Stout/Porter</h5>
	<p class="par3 karolioText2 title4 colorBrown">A family of very dark beers characterized by dark chocolate malt flavours/scents and rather aggressive hop bitterness. Opaque, rigid body.
</p>
<p class="par4 karolioText2 title4 colorBrown">Porter – dark, black or black and brown top-fermented beer. Clearly expressed dark malt beer characterized by chocolate and coffee aftertaste, sweeter than the stout type.
</p>

<p class="par4 karolioText2 title4 colorBrown">Stout – it originates from porter and differs in greater strength. Drier, darker, stronger body, with typical coffee aftertaste. Produced with heavily roasted barley malt, providing the black colour, typical carbon chalky sensation and bitterness somewhat different than in other beers.
</p>

<img src="images2/stout.png" class="myImg1 myImg1over0">
<img src="images2/stout2.png" class="myImg1 myImg1over111">
<img src="images2/porter.png" class="myImg1 myImg1over11">
</div>
<hr>
<div class="alestext3 title4 colorBrown">
	<h5>Pale Ale</h5>
	<p class="par3 karolioText2 title4 colorBrown">

Beer originating in Britain, made of light malt, often referred to as a golden or light ale. Characteristic of malt sweetness while hop flavour ranges from very mild to strong. Malt flavour is more emphasised in some types while in others it is overshadowed by hops. This type of beer is characterized by an average body strength. Pale ale can be combined with a very broad palette of flavours – from spicy Indian cuisine to a variety of meat, fish and poultry dishes. Bright hop tunes perfectly complement the savoury and spicy taste of meals. Bitterness of hops contrasts with the smoky flavour of charcoal-grilled dishes. It blends with blue cheeses.

</p>
<img src="images2/palez.png" class="myImg2 palez">
<img src="images2/palez2.png" class="myImg2 palez">
<img src="images2/palez4.png" class="myImg2 palez">
<img src="images2/palez5.png" class="myImg2 palez">
</div>
<hr>
<div class="alestext2 title4 colorBrown">
	<h5>German Ale</h5>
	<p class="par3 karolioText2 title4 colorBrown">

Germany has two classic beer types attributable to ale, that are considered as ale is not because of their colour, strength or hop aroma, but only for the ale yeast that is used. This is a hybrid beer, which is neither typical ale nor typical lager. To put it simply – ale is fermented at colder temperatures than normal ales, and lager – in a warmer environment than it should be. It provides unique properties of beer, which fascinates the lovers of unconventional, more interesting beers.

Germany is famous for its wheat beers, with green apple acidity and unique notes of banana and clove. Wheat beer type originates from Bavaria. They named it white back in the Middle Ages, because the colour of fresh light wheat malt was very different from the barley malt. The colour ranges from light yellow to rich amber, flavour its intense, rich with malt, fruit and spicy clove aroma.


</p>
<img src="images2/gerale.png" class="myImg1">
<img src="images2/gerale2.png" class="myImg1 myImg101">
<img src="images2/gerale3.png" class="myImg1 myImg102">
<img src="images2/gerale4.png" class="myImg1 myImg103">
</div>
</div>
</div>



 <!--jquery-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
 <!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/js/materialize.min.js"></script>
<!--jquery mani + savo ikelkit-->
<script src="jscriptjura.js" type="text/javascript"></script>
</body>
</html>

<?php include('footer.php'); ?>